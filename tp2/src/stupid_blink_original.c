/*
 * stupid_blink.c
 *
 *  Created on: Oct 8, 2015
 *      Author: peter
 */

#include "peripheral/ports/plib_ports.h"


void stupid_blink(void){
	// configure LEDs for output
	PLIB_PORTS_DirectionOutputSet(PORTS_ID_0,PORT_CHANNEL_B,1<<PORTS_BIT_POS_12 | 1<< PORTS_BIT_POS_15);

	PLIB_PORTS_PinSet(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_12);
	PLIB_PORTS_PinClear(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_15);


	while(1){
		PLIB_PORTS_PinToggle(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_12);
		PLIB_PORTS_PinToggle(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_15);
		// waiting by counting, this is stupid!
		volatile int toto;
		for(toto=0;toto<800000;toto++);
	}

}
