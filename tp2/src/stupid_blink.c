/*
 * stupid_blink.c
 *
 *  Created on: Oct 8, 2015
 *      Author: peter
 */

#include "peripheral/ports/plib_ports.h"
#include "peripheral/tmr/plib_tmr.h"
#include "peripheral/int/plib_int.h"
#include <sys/attribs.h>

void __ISR(_TIMER_3_VECTOR, IPL1SOFT) isr_timer_2_3(void) {
  PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_3);
  PLIB_PORTS_PinToggle(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_12); // orange
}
void __ISR(_TIMER_5_VECTOR, IPL1SOFT) isr_timer_4_5(void) {
  PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_5);
  PLIB_PORTS_PinToggle(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_15); // green
}


void setup_timer_2_3() {
  // setup 32 timer 2-3
  PLIB_TMR_Stop (TMR_ID_2);
  PLIB_TMR_ClockSourceSelect(TMR_ID_2, TMR_CLOCK_SOURCE_PERIPHERAL_CLOCK);
  PLIB_TMR_PrescaleSelect(TMR_ID_2, TMR_PRESCALE_VALUE_256);
  PLIB_TMR_Mode32BitEnable(TMR_ID_2);  // pas necessaire 'opt'
  PLIB_TMR_Counter32BitClear(TMR_ID_2);  // pas necessaire 'opt'
  PLIB_TMR_Period32BitSet(TMR_ID_2, 125000); // 2.5 times/sec so 0.4sec period
  // prepare ISR
  PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_3);
  PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_3); // pour activer le ISR 
  PLIB_INT_VectorPrioritySet(INT_ID_0, INT_VECTOR_T3, INT_PRIORITY_LEVEL1); //donner prio
}
void setup_timer_4_5() {
  // setup 32 timer 4-5
  PLIB_TMR_Stop (TMR_ID_4);
  PLIB_TMR_ClockSourceSelect(TMR_ID_4, TMR_CLOCK_SOURCE_PERIPHERAL_CLOCK);
  PLIB_TMR_PrescaleSelect(TMR_ID_4, TMR_PRESCALE_VALUE_256);
  PLIB_TMR_Mode32BitEnable(TMR_ID_4);  // pas necessaire 'opt'
  PLIB_TMR_Counter32BitClear(TMR_ID_4);  // pas necessaire 'opt'
  PLIB_TMR_Period32BitSet(TMR_ID_4, 312500); // 1 second period
  // prepare ISR
  PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_5);
  PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_5);
  PLIB_INT_VectorPrioritySet(INT_ID_0, INT_VECTOR_T5, INT_PRIORITY_LEVEL1);
}


void stupid_blink(void) {
  // configure LEDs for output
  PLIB_PORTS_DirectionOutputSet(PORTS_ID_0,PORT_CHANNEL_B,1<<PORTS_BIT_POS_12 | 1<< PORTS_BIT_POS_15);
  PLIB_PORTS_PinSet(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_12);
  PLIB_PORTS_PinClear(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_15);

  setup_timer_2_3();
  PLIB_TMR_Start(TMR_ID_2);

  setup_timer_4_5();
  PLIB_TMR_Start(TMR_ID_4);

  while(1) {
  }
}
