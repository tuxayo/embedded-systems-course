/*
 * stupid_blink.c
 *
 *  Created on: Oct 8, 2015
 *      Author: peter
 */

#include "peripheral/ports/plib_ports.h"
#include "peripheral/tmr/plib_tmr.h"
#include "peripheral/int/plib_int.h"
#include <sys/attribs.h>

void __ISR(_TIMER_1_VECTOR, IPL1SOFT) empty_ISR(void) {
}


void stupid_blink(void){
  // configure LEDs for output
  PLIB_PORTS_DirectionOutputSet(PORTS_ID_0,PORT_CHANNEL_B,1<<PORTS_BIT_POS_12 | 1<< PORTS_BIT_POS_15);
  PLIB_PORTS_PinSet(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_12); // orange
  PLIB_PORTS_PinClear(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_15); // green

  // configuration et initialisation du timer
  PLIB_TMR_Stop (TMR_ID_1);   // je prend le timer_0
  PLIB_TMR_ClockSourceSelect(TMR_ID_1, TMR_CLOCK_SOURCE_PERIPHERAL_CLOCK);
  PLIB_TMR_PrescaleSelect(TMR_ID_1, TMR_PRESCALE_VALUE_256);
  PLIB_TMR_Mode16BitEnable(TMR_ID_1);  // pas necessaire 'opt'
  PLIB_TMR_Counter16BitClear(TMR_ID_1);  // pas necessaire 'opt'
  PLIB_TMR_Period16BitSet(TMR_ID_1, 31250);
  PLIB_TMR_Start(TMR_ID_1);

  // prepare flag
  PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_1);
  PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_1);
  PLIB_INT_VectorPrioritySet(INT_ID_0, INT_VECTOR_T1, INT_PRIORITY_LEVEL1);

  while(1) {
    PLIB_PORTS_PinToggle(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_12); // orange
    PLIB_PORTS_PinToggle(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_15); // green
    sleep();
  }
}

void sleep() {
  while(PLIB_INT_SourceFlagGet(INT_ID_0, INT_SOURCE_TIMER_1) == 0) {
  }
  PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_1);
}
