/*
 * conventions: tabs de taille 4
 */

#include "peripheral/ports/plib_ports.h"
#include "peripheral/tmr/plib_tmr.h"
#include "peripheral/int/plib_int.h"
#include "peripheral/spi/plib_spi.h"
#include <sys/attribs.h>
#include <stdlib.h>

#include "ws2812b_translation.h"
#define TIME_SLEEP 500

#define MY_SPI_ID            SPI_ID_3
#define PERIPHERAL_BUS_CLK   MY_CLOCK_FREQUENCY
#define SPI_BAUD_RATE        2500000 // 1/periode
// periode: 400ns (calculé en sais pas comment) == 32 cycles aussi
#define MY_CLOCK_FREQUENCY   80000000 // 80MHz

#define BUFF_SIZE   106 // reset(16 octets) + 10 leds (90 octets)


uint32_t buffer_32[BUFF_SIZE/4];
uint8_t* buffer_8 = (uint8_t*)buffer_32;
int current_buffer_8_index = 0;

inline void wait_end_period(){
	while(PLIB_INT_SourceFlagGet(INT_ID_0, INT_SOURCE_TIMER_2) == 0); // On regarde le flag si à 0 on continue
	PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_2);
}


void sleep(int milliseconds) {
	// 200 milliseconds -> (80e6/32)/1000*200
	int i;
	int number_or_periods = (80e6/32)/1000*milliseconds;
	for(i = 0; i< number_or_periods; i++) {
		wait_end_period();
	}
}

/*inline void write_SPI_buffer(uint8_t data) {
	while (PLIB_SPI_TransmitBufferIsFull(MY_SPI_ID));
	PLIB_SPI_BufferWrite (MY_SPI_ID, data);
}
*/

inline void send_buffer_to_SPI() {
	int i;
	for (i = 0; i < BUFF_SIZE/4; ++i) {
		while (PLIB_SPI_TransmitBufferIsFull(MY_SPI_ID));
		PLIB_SPI_BufferWrite32bit (MY_SPI_ID, buffer_32[i]);
	}
	current_buffer_8_index = 0;
}

inline void write_in_our_buffer(uint8_t data) {
	buffer_8[(current_buffer_8_index) ^ 3] = data;
	current_buffer_8_index ++ ;

}


inline void reset() {
	int i;
	// il faut 125 zeros (des vrai zero, des bits, pas 101)
	// donc ~16 bytes
	for(i=0;i<16;i++)
		write_in_our_buffer(0);
}

inline void send_subpixel(int intensity) {
	write_in_our_buffer(byte1[intensity]);
	write_in_our_buffer(byte2[intensity]);
	write_in_our_buffer(byte3[intensity]);
}

inline void send_color(int red, int green, int blue) {
	// send one color
	send_subpixel(green);
	send_subpixel(red);
	send_subpixel(blue);
}

inline void send_black() {
	send_color(0,0,0);
}

inline void send_white() {
	send_color(255,255,255);
}

void setup_timer_2() {
	PLIB_TMR_Stop (TMR_ID_2); // toujours au debut **important**
	PLIB_TMR_ClockSourceSelect(TMR_ID_2, TMR_CLOCK_SOURCE_PERIPHERAL_CLOCK); // check TIMER_2
	PLIB_TMR_PrescaleSelect(TMR_ID_2, TMR_PRESCALE_VALUE_1); // defini un Prescale entre 1-256, là on est à 1
	PLIB_TMR_Period16BitSet(TMR_ID_2, 32); // defini une Periode 32 sur 16 bits
	PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_2);  // remettre à 0 le flag
}

inline void setup_SPI() {
	PLIB_SPI_BaudRateSet(MY_SPI_ID, MY_CLOCK_FREQUENCY, SPI_BAUD_RATE);
	PLIB_SPI_CommunicationWidthSelect(MY_SPI_ID, SPI_COMMUNICATION_WIDTH_32BITS);
	PLIB_SPI_Enable(MY_SPI_ID);
	PLIB_SPI_FIFOEnable(MY_SPI_ID);
	PLIB_SPI_MasterEnable (MY_SPI_ID);
}

void setup_debug_leds() {
	PLIB_PORTS_DirectionOutputSet(PORTS_ID_0,PORT_CHANNEL_B,1<<PORTS_BIT_POS_12 | 1<< PORTS_BIT_POS_15);
}

inline void clignote(){
	int j,k,l,m;
	for (j = 0; j <= 255 ; j++) {
		reset();
		send_color(j,0,0);
		sleep(TIME_SLEEP);
		send_black();
		sleep(TIME_SLEEP);

		for (k = 150; k <= 255 ; k++) {
			reset();
			send_color(k,k,0);
			sleep(TIME_SLEEP);
			send_black();
			sleep(TIME_SLEEP);
			for (l = 100; l <= 255 ; l++) {
				reset();
				send_color(l,l,0);
				sleep(TIME_SLEEP);
				send_black();
				sleep(TIME_SLEEP);
				for (m = TIME_SLEEP; m <= 255 ; m++) {
					reset();
					send_color(m,m,m);
					sleep(TIME_SLEEP);
					send_black();
					sleep(TIME_SLEEP);
				}

			}
		}
	}
}

inline void send_data() {
	int i;
	//for(i = 0; i < 300; ++i) {
		reset();
		send_color(0,255,0);
		send_color(255,0,0);
		send_color(255,0,0);
		send_color(0,0,255);
		send_color(rand()%256,rand()%256,0);
		send_color(rand()%256,rand()%256,0);
		send_color(rand()%256,(i+10)%256,0);
		send_color(i,rand()%256,100);
		send_color(255,255,255);
		send_color(255,255,255);
		send_buffer_to_SPI();
		sleep(TIME_SLEEP);
	/*	reset();
		send_black();
		send_black();
		send_black();
		send_black();
		send_black();
		send_black();
		send_black();
		send_black();
		send_black();
		send_black();
		sleep(TIME_SLEEP);
		send_buffer_to_SPI();
		*/
	//}
	//clignote();
}

void main_led(void) {
	setup_debug_leds ();
	PLIB_PORTS_PinSet(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_12); // orange
	setup_SPI();
	setup_timer_2();
	PLIB_TMR_Start(TMR_ID_2); // start

	send_data();

	PLIB_PORTS_PinSet(PORTS_ID_0,PORT_CHANNEL_B,PORTS_BIT_POS_15); // green

}
