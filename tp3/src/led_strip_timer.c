/*
 * stupid_blink.c
 *
 *  Created on: Oct 8, 2015
 *  Author: peter
 */

#include "peripheral/ports/plib_ports.h"
#include "peripheral/tmr/plib_tmr.h"
#include "peripheral/int/plib_int.h"
#include <sys/attribs.h>


inline void wait_end_period(){
	while(PLIB_INT_SourceFlagGet(INT_ID_0, INT_SOURCE_TIMER_2) == 0); // On regarde le flag si à 0 on continue
	PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_2);
}


void sleep() {
	// 0.5 second (80e6/32)*0.5
	int i;
	for(i = 0; i< 2500000; i++){
		wait_end_period();
	}
}

void setup_timer_2() {
	PLIB_TMR_Stop (TMR_ID_2); // toujours au debut **important**
	PLIB_TMR_ClockSourceSelect(TMR_ID_2, TMR_CLOCK_SOURCE_PERIPHERAL_CLOCK); // check TIMER_2
	PLIB_TMR_PrescaleSelect(TMR_ID_2, TMR_PRESCALE_VALUE_1); // defini un Prescale entre 1-256, là on est à 1
	PLIB_TMR_Period16BitSet(TMR_ID_2, 32); // defini une Periode 32 sur 16 bits
	PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_2);  // remettre à 0 le flag
}

inline void reset() {
	PLIB_PORTS_PinWrite(PORTS_ID_0,PORT_CHANNEL_D,PORTS_BIT_POS_3,0);
	int i;
	for(i=0;i<125;i++)
		wait_end_period();
}
void config_port_pin(){
	// configure pin for output
	PLIB_PORTS_DirectionOutputSet(PORTS_ID_0,PORT_CHANNEL_D,1<<PORTS_BIT_POS_3); // SDO3, pin 8 sur carte
	PLIB_PORTS_PinSet(PORTS_ID_0,PORT_CHANNEL_D,PORTS_BIT_POS_3);
	PLIB_PORTS_PinClear(PORTS_ID_0,PORT_CHANNEL_D,PORTS_BIT_POS_3);

}

inline void send_bit_one() {
	PLIB_PORTS_PinWrite(PORTS_ID_0,PORT_CHANNEL_D,PORTS_BIT_POS_3,1);
	wait_end_period();
	wait_end_period();
	PLIB_PORTS_PinWrite(PORTS_ID_0,PORT_CHANNEL_D,PORTS_BIT_POS_3,0);
	wait_end_period();
}

inline void send_bit_zero() {
	PLIB_PORTS_PinWrite(PORTS_ID_0,PORT_CHANNEL_D,PORTS_BIT_POS_3,1);
	wait_end_period();
	PLIB_PORTS_PinWrite(PORTS_ID_0,PORT_CHANNEL_D,PORTS_BIT_POS_3,0);
	wait_end_period();
	wait_end_period();
}

inline void send_white() {
	// send one color
	int i;
	for(i = 0; i <= 24; i++){
		send_bit_one();
	}
}


inline void send_black() {
	// send one color
	int i;
	for(i = 0; i<= 24; i++){
		send_bit_zero();
	}
}

inline void send_rgb(int r, int g, int b) {
	// send one color

	int i;
	for(i = 0; i < 8; i++) {
		if (g) {
			send_bit_one();
		}else send_bit_zero();
	}
	for(i = 0; i < 8; i++) {
		if (r) {
			send_bit_one();
		} else send_bit_zero();
	}
	for(i = 0; i < 8; i++) {
		if (b){
			send_bit_one();
		} else send_bit_zero();
	}

}

void stupid_blink(void) {
	config_port_pin();
	setup_timer_2();
	PLIB_TMR_Start(TMR_ID_2); // start
	reset(); // debut, voir doc

	int i;
	int j;
	for (i = 0; i < 5; ++i) {
		send_rgb(1,0,0);
		send_rgb(1,0,0);
		send_rgb(0,1,0);
		send_rgb(0,1,0);
		send_rgb(0,0,1);
		send_rgb(0,0,1);

		sleep();

		send_black();
		send_black();
		send_black();
		send_black();
		send_black();
		send_black();
		sleep();
	}
	send_rgb(1,0,0);
	send_rgb(1,0,0);
	send_rgb(0,1,0);
	send_rgb(0,1,0);
	send_rgb(0,0,1);
	send_rgb(0,0,1);

}
